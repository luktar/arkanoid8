﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

[Serializable]
public class Brick
{
    [XmlAttribute]
    public BrickType Type { get; set; }

    [XmlAttribute]
    public float X { get; set; }

    [XmlAttribute]
    public float Y { get; set; }

    public Brick()
    {

    }

    public Brick(float x, float y, BrickType brickType)
    {
        X = x;
        Y = y;
        Type = brickType;
    }
}

