﻿using Assets.Scripts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;
using Random = UnityEngine.Random;

public class Level
{
    [XmlIgnore]
    private List<BrickType> RandomBrickTypes { get; set; }

    [XmlAttribute]
    public string PlayerName { get; set; }

    [XmlAttribute]
    public int LevelNumber { get; set; }

    [XmlAttribute]
    public int LivesCount { get; set; }

    [XmlAttribute]
    public int Score { get; set; }

    public List<Brick> Bricks { get; set; }

    public Level() {
        RandomBrickTypes = new List<BrickType>()
        {
            BrickType.LiveBrick,
            BrickType.LiveBrick,
            BrickType.DoubleScoreBrick,
            BrickType.DoubleScoreBrick,
            BrickType.SlowMotionBrick,
            BrickType.SlowMotionBrick,
            BrickType.RegularBrick,
            BrickType.RegularBrick,
            BrickType.RegularBrick,
            BrickType.RegularBrick
        };
    }

    public bool Exists()
    {
        return File.Exists(GetLevelPath());
    }

    public void Initialize()
    {
        Bricks = new List<Brick>();

        for (int row = 0; 
            row < Constants.HORIZONTAL_LEVEL_SIZE; 
            row += Constants.HORIZONTAL_BRICK_SIZE)
        {
            for (int column = 0;
                column < Constants.VERTICAL_LEVEL_SIZE;
                column += Constants.VERTICAL_BRICK_SIZE)
            {
                float xPosition = row + Constants.HORIZONTAL_LEVEL_START;
                float yPosition = column + Constants.VERTICAL_LEVEL_START;

                BrickType currentBrickType = GetBrickType();

                if(currentBrickType == BrickType.RegularBrick)
                    Bricks.Add(new Brick(xPosition, yPosition, BrickType.RegularBrick));
                else if(currentBrickType == BrickType.LiveBrick)
                    Bricks.Add(new Brick(xPosition, yPosition, BrickType.LiveBrick));
                else if(currentBrickType == BrickType.DoubleScoreBrick)
                    Bricks.Add(new Brick(xPosition, yPosition, BrickType.DoubleScoreBrick));
                else
                    Bricks.Add(new Brick(xPosition, yPosition, BrickType.SlowMotionBrick));
            }
        }
    }

    public void Remove()
    {
        if(Exists())
        {
            File.Delete(GetLevelPath());
        }
    }

    public void Load()
    {
        string fileName = GetLevelPath();

        Level level = SerializerHelper.DeserializeObject<Level>(fileName);

        PlayerName = level.PlayerName;
        Score = level.Score;
        Bricks = level.Bricks;
        LivesCount = level.LivesCount;
        LevelNumber = level.LevelNumber;
    }

    public void Instantiate(GameManager gameManager)
    {
        foreach(Brick brick in Bricks)
        {
            GameObject currentGameObject;
            if(brick.Type == BrickType.RegularBrick)
            {
                currentGameObject = 
                    UnityEngine.Object.Instantiate(
                        gameManager.regularBrick, new Vector3(brick.X, brick.Y, 0), Quaternion.identity);
            }
            else if (brick.Type == BrickType.LiveBrick)
            {
                currentGameObject = 
                    UnityEngine.Object.Instantiate(
                        gameManager.liveBrick, new Vector3(brick.X, brick.Y, 0), Quaternion.identity);
            }
            else if (brick.Type == BrickType.DoubleScoreBrick)
            {
                currentGameObject = 
                    UnityEngine.Object.Instantiate(
                        gameManager.doubleScoreBrick, new Vector3(brick.X, brick.Y, 0), Quaternion.identity);
            }
            else
            {
                currentGameObject = 
                    UnityEngine.Object.Instantiate(
                        gameManager.slowMotionBrick, new Vector3(brick.X, brick.Y, 0), Quaternion.identity);
            }
            BrickBase brickBase = currentGameObject.GetComponent<BrickBase>();
            brickBase.SourceBrick = brick;
        }
    }

    public void DrawWall(GameObject wall)
    {
        for(int i = 0; i < Constants.WALL_HEIGHT; i++)
        {
            float value = i + Constants.WALL_BOTTOM;

            UnityEngine.Object.Instantiate(wall, new Vector3(Constants.LEFT_WALL_X, value, 0), Quaternion.identity);
            UnityEngine.Object.Instantiate(wall, new Vector3(Constants.RIGHT_WALL_X, value, 0), Quaternion.identity);
        }

        for(int i = 0; i < Constants.WALL_WIDTH; i++)
        {
            float value = i + Constants.WALL_LEFT_BOARD;

            UnityEngine.Object.Instantiate(wall, new Vector3(value, Constants.TOP_WALL_Y, 0), Quaternion.identity);
        }
    }

    private string GetLevelPath()
    {
        return Path.Combine(AssemblyHelper.GetAssemblyDirectory(),
                Constants.LEVEL_FILE_NAME);
    }

    public void Save()
    {
        string saveFileName = GetLevelPath();
        SerializerHelper.SerializeObject(saveFileName, this);
    }

    private BrickType GetBrickType()
    {
        return RandomBrickTypes[Random.Range(0, 10)];
    }
}

