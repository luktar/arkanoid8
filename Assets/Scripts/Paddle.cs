﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour {

    
    private Vector3 playerPos = new Vector3(0, 0, 0);
    private bool isPause = false;

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {

        if (!isPause)
        {
            float paddleSpeed = Constants.PADDLE_SPEED * Time.timeScale;

            float xPos = transform.position.x +
                (Input.GetAxis(Constants.HORIZONTAL) * paddleSpeed);

            playerPos = new Vector3(
                Mathf.Clamp(xPos, -Constants.HORIZONTAL_MOVEMENT, Constants.HORIZONTAL_MOVEMENT),
                Constants.VERTICAL_PADDLE_POSITION, 0);
            transform.position = playerPos;
        }
    }

    public void OnPauseGame()
    {
        isPause = true;
    }

    public void OnResumeGame()
    {
        isPause = false;
    }
}
