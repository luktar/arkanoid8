﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegularBrick : BrickBase
{
    protected override void PerformAction()
    {
        GameManager.instance.IncreaseScore();
    }
}
