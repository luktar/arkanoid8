﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public abstract class BrickBase : MonoBehaviour
{
    public Brick SourceBrick { get; set; }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
        GameManager.instance.RemoveBrick(SourceBrick);
        PerformAction();
        GameManager.instance.CheckGameComplete();
    }

    protected abstract void PerformAction();
}

