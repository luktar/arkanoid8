﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class SlowMotionBrick : BrickBase
{
    protected override void PerformAction()
    {
        GameManager.instance.PerformSlowMotion();
    }
}

