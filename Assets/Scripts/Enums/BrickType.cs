﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

[Serializable]
public enum BrickType
{
    [XmlEnum("RegularBrick")]
    RegularBrick,
    [XmlEnum("DoubleScoreBrick")]
    DoubleScoreBrick,
    [XmlEnum("LiveBrick")]
    LiveBrick,
    [XmlEnum("SlowMotionBrick")]
    SlowMotionBrick
}

