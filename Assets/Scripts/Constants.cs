﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class Constants
    {
        public const string MAIN_MENU_SCENE_NAME = "MainMenu";
        public const string GAME_SCENE_NAME = "Game";
        public const string PRESS_ESC_EXIT_GAME = "Press ESC to exit...";
        public const string BEST_SCORES = "High scores:";
        public const string NEW_LINE = "\n";
        public const string LEVEL_NUMBER_FORMATTER = "00";
        public const string SCORE_FORMATTER = "000000";
        public const string DOTS = "........";
        public const string DEFAULT_PLAYER_NAME = "Player 1";
        public const string HORIZONTAL = "Horizontal";
        public const string FIRE_1 = "Fire1";
        public const string LEVEL_FILE_NAME = "Level.xml";
        public const string LEADERBOARD_FILE_NAME = "Leaderboard.xml";
        public const string LIVES = "Lives: ";
        public const string SCORE = "Score: ";
        public const string LEVEL = "Level: ";
        public const string BALL = "Ball";

        public const int MAX_PLAYER_NAME_LENGTH = 8;

        public const float BALL_INITIAL_VELOCITY = 600f;

        public const float HORIZONTAL_MOVEMENT = 7.5f;
        public const float VERTICAL_PADDLE_POSITION = -7f;
        public const float PADDLE_SPEED = 0.55f;
        public const int SLOW_MOTION_PERIOD = 5000; // sec.

        public const float LEFT_WALL_X = -9.5f;
        public const float RIGHT_WALL_X = 9.5f;
        public const float TOP_WALL_Y = 7.5f;
        public const int WALL_HEIGHT = 18;
        public const int WALL_WIDTH = 18;
        public const float WALL_BOTTOM = -9.5f;
        public const float WALL_LEFT_BOARD = -8.5f;

        public const int INITIAL_LIVES_COUNT = 3;
        public const int REGULAR_SCORE_COUNT = 100;
        public const int VERTICAL_LEVEL_SIZE = 3;
        public const int HORIZONTAL_LEVEL_SIZE = 14;
        public const int HORIZONTAL_BRICK_SIZE = 2;
        public const int VERTICAL_BRICK_SIZE = 1;
        public const float VERTICAL_LEVEL_START = 3.5f;
        public const float HORIZONTAL_LEVEL_START = -6f;

        public const float SLOW_MOTION_MULTIPLiER = 0.6f;
    }
}
