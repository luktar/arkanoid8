﻿using Assets.Scripts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

[Serializable]
public class LeaderBoard
{
    [XmlAttribute]
    public string PlayerName { get; set; }

    [XmlArray]
    public List<LeaderBoardRecord> LeaderBoardRecords { get; set; }

    public LeaderBoard() { }

    public void Load()
    {
        string leaderboardPath = GetLeaderboardPath();

        if(File.Exists(leaderboardPath))
        {
            LeaderBoard currentLeaderBoard =
                SerializerHelper.DeserializeObject<LeaderBoard>(leaderboardPath);
            LeaderBoardRecords = currentLeaderBoard.LeaderBoardRecords;
            PlayerName = currentLeaderBoard.PlayerName;
            return;
        }
        CreateLeaderboard();
            
    }

    private void CreateLeaderboard()
    {
        PlayerName = Constants.DEFAULT_PLAYER_NAME;
        LeaderBoardRecords = new List<LeaderBoardRecord>();
        for(int i = 0; i < 10; i++)
        {
            LeaderBoardRecords.Add(new LeaderBoardRecord(
                Constants.DOTS, 0));
        }

        Save();
    }

    private string GetLeaderboardPath()
    {
        return Path.Combine(AssemblyHelper.GetAssemblyDirectory(), Constants.LEADERBOARD_FILE_NAME); ;
    }

    public void Save()
    {
        string leaderboardPath = GetLeaderboardPath();

        SerializerHelper.SerializeObject(leaderboardPath, this);
    }

    private void SortLeaderboard()
    {
        LeaderBoardRecords = LeaderBoardRecords.OrderByDescending(i => i.Score).ToList();
    }

    public void Add(string playerName, int score)
    {
        SortLeaderboard();

        if(score > LeaderBoardRecords.Last().Score)
        {
            LeaderBoardRecords.Remove(LeaderBoardRecords.Last());
            LeaderBoardRecords.Add(new LeaderBoardRecord(playerName, score));
            SortLeaderboard();
        }

        Save();
    }

    public override string ToString()
    {
        string scores = Constants.BEST_SCORES + Constants.NEW_LINE + Constants.NEW_LINE;

        foreach (LeaderBoardRecord record in LeaderBoardRecords)
        {
            string playerName = record.PlayerName;

            if (playerName.Length < Constants.MAX_PLAYER_NAME_LENGTH + 1)
            {
                for (int i = playerName.Length; i < Constants.MAX_PLAYER_NAME_LENGTH; i++)
                {
                    playerName += ".";
                }
            }

            scores += playerName + "  " + record.Score.ToString(Constants.SCORE_FORMATTER) + Constants.NEW_LINE;
        }

        scores += Constants.NEW_LINE + Constants.PRESS_ESC_EXIT_GAME;
     
        return scores;
    }
}

