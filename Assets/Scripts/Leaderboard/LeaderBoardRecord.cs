﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

[Serializable]
public class LeaderBoardRecord
{
    [XmlAttribute]
    public string PlayerName { get; set; }

    [XmlAttribute]
    public int Score { get; set; }

    public LeaderBoardRecord() { }

    public LeaderBoardRecord(string playerName, int score)
    {
        PlayerName = playerName;
        Score = score;
    }
}

