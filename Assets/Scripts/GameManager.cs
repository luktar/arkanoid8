﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public Button backToMainMenuButton;

    public Canvas hudCanvas;
    public Canvas pauseMenuCanvas;
    public Canvas levelCanvas;
    public Canvas gameOverCanvas;

    public Text score;
    public Text lives;
    public Text level;
    public Text playerName;
    public Text levelNumberText;

    public Text leaderBoardText;

    public GameObject wall;
    public GameObject paddle;

    public GameObject regularBrick;
    public GameObject liveBrick;
    public GameObject doubleScoreBrick;

    public GameObject slowMotionBrick;

    private GameObject clonePaddle;
    private GameObject cloneBall;

    private LeaderBoard leaderBoard;

    public Level Level { get; private set; }
    private long SlowMotionEnd { get; set; }

    public static GameManager instance = null;

    public int bricksCount;

    public float resetDelay = 1f;
    private HashSet<KeyCode> PressedButtons = new HashSet<KeyCode>();

    private bool blockInput = false;
    private bool gameOverActive = false;

	// Use this for initialization
	void Awake () {

        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        leaderBoard = new LeaderBoard();
        leaderBoard.Load();

        backToMainMenuButton.onClick.AddListener(
            BackToMainMenuOnClick);

        SetupGame();
    }

    private void BackToMainMenuOnClick()
    {
        Level.Save();
        SceneManager.LoadScene(Constants.MAIN_MENU_SCENE_NAME);
    }

    public void SetupGame()
    {
        SlowMotionEnd = DateTime.Now.Ticks;

        Level = new Level();
        Level.DrawWall(wall);

        if (MenuManager.newGamePressed)
        {
            NewGamePressed();
            ShowNextLevel();
            return;
        }
        ReturnToGamePressed();
        ShowNextLevel();
    }

    private void ShowNextLevel()
    {
        levelNumberText.text = Constants.LEVEL + (1 + Level.LevelNumber).ToString(Constants.LEVEL_NUMBER_FORMATTER);
        blockInput = true;
        hudCanvas.gameObject.SetActive(false);
        levelCanvas.gameObject.SetActive(true);

        Invoke("HideNextLevel", 2);
    }

    private void HideNextLevel()
    {
        blockInput = false;
        hudCanvas.gameObject.SetActive(true);
        levelCanvas.gameObject.SetActive(false);
    }

    public void NewGamePressed()
    {
        Level.LivesCount = Constants.INITIAL_LIVES_COUNT;
        Level.Score = 0;
        Level.PlayerName = MenuManager.playerName;
        Level.LevelNumber = 0;

        Level.Initialize();
        Level.Instantiate(this);

        SetupPaddle();

        SetPlayerNameText();

        UpdateLives();
        UpdateScore();
        UpdateLevel();
    }

    private void SetPlayerNameText()
    {
        playerName.text = MenuManager.playerName;
    }

    private void ReturnToGamePressed()
    {
        Level.Load();
        Level.Instantiate(this);

        MenuManager.playerName = Level.PlayerName;

        SetupPaddle();

        SetPlayerNameText();

        UpdateLives();
        UpdateScore();
        UpdateLevel();
    }

    public void LoadNextLevel()
    {
        SetupPaddle();
        Level.LevelNumber++;

        Level.Initialize();
        Level.Instantiate(this);

        UpdateLives();
        UpdateScore();
        UpdateLevel();
    }

    private void BackToMainMenuPressed()
    {
        Level.Save();
    }

    public void IncreaseScore()
    {
        Level.Score += Constants.REGULAR_SCORE_COUNT;
        UpdateScore();
    }

    public void DoubleIncreseScore()
    {
        Level.Score += 2 * Constants.REGULAR_SCORE_COUNT;
        UpdateScore();
    }

    public void AddLive()
    {
        Level.LivesCount += 1;
        UpdateLives();
    }

    bool isPaused = false;

    // Update is called once per frame
    void Update () {

        if (blockInput) return;

        if (Input.GetKeyDown(KeyCode.Escape))         
        {
            if(gameOverActive)
            {
                SceneManager.LoadScene(Constants.MAIN_MENU_SCENE_NAME);
                return;
            }

            Pause();
        }

        CheckSlowMotion();
    }

    private void Pause()
    {
        Ball ball = cloneBall.GetComponent<Ball>();
        Paddle paddle = clonePaddle.GetComponent<Paddle>();
        if (!isPaused)
        {
            ball.OnPauseGame();
            paddle.OnPauseGame();
            hudCanvas.gameObject.SetActive(false);
            pauseMenuCanvas.gameObject.SetActive(true);
            isPaused = true;
        }
        else
        {
            ball.OnResumeGame();
            paddle.OnResumeGame();
            hudCanvas.gameObject.SetActive(true);
            pauseMenuCanvas.gameObject.SetActive(false);
            isPaused = false;
        }
    }

    private void CheckSlowMotion()
    {
        if (DateTime.Now.Ticks < SlowMotionEnd)
        {
            Time.timeScale = Constants.SLOW_MOTION_MULTIPLiER;
        }
        else
        {
            Time.timeScale = 1f;
        }
    }

    public void RemoveBrick(Brick sourceBrick)
    {
        Level.Bricks.Remove(sourceBrick);
    }

    private void DestroyPaddle()
    {
        DestroyObject(cloneBall);
        DestroyObject(clonePaddle);
    }

    private void SetupPaddle()
    {
        clonePaddle = Instantiate(paddle, transform.position, Quaternion.identity) as GameObject;
        foreach(Transform child in clonePaddle.transform)
        {
            if (child.name == Constants.BALL)
            {
                cloneBall = child.gameObject;
            }
        }
    }

    public void CheckGameComplete()
    {
        if (Level.Bricks.Count < 1)
        {       
            RemoveSlowMotion();
            DestroyPaddle();
            LoadNextLevel();
            ShowNextLevel();
        }
    }

    public void LoseLife()
    {
        RemoveSlowMotion();
        Level.LivesCount--;
        UpdateLives();
        DestroyPaddle();

        if (Level.LivesCount < 1)
        {
            GameOver();
            return;
        }

        SetupPaddle();
    }

    private void GameOver()
    {
        leaderBoard.Add(
            Level.PlayerName,
            Level.Score);
        leaderBoard.Load();

        Level.Remove();
        gameOverActive = true;

        leaderBoardText.text = leaderBoard.ToString();

        hudCanvas.gameObject.SetActive(false);
        gameOverCanvas.gameObject.SetActive(true);
    }

    /// <summary>
    /// https://docs.microsoft.com/en-us/dotnet/api/system.datetime.ticks?f1url=https%3A%2F%2Fmsdn.microsoft.com%2Fquery%2Fdev15.query%3FappId%3DDev15IDEF1%26l%3DEN-US%26k%3Dk(System.DateTime.Ticks);k(TargetFrameworkMoniker-.NETFramework,Version%3Dv3.5);k(DevLang-csharp)%26rd%3Dtrue&view=netframework-4.7
    /// </summary>
    public void PerformSlowMotion()
    {
        // Add 5 sec.
        SlowMotionEnd = DateTime.Now.Ticks + (Constants.SLOW_MOTION_PERIOD * 10000);
    }

    private void RemoveSlowMotion()
    {
        SlowMotionEnd = DateTime.Now.Ticks;
    }

    private void UpdateScore()
    {
        score.text = Constants.SCORE + Level.Score.ToString(Constants.SCORE_FORMATTER);
    }

    public void UpdateLives()
    {
        lives.text = Constants.LIVES + Level.LivesCount.ToString("00");
    }

    public void UpdateLevel()
    {
        level.text = Constants.LEVEL + (Level.LevelNumber + 1).ToString(Constants.LEVEL_NUMBER_FORMATTER);
    }
}
