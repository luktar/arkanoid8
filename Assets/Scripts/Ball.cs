﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Ball : MonoBehaviour {

    private Vector3 savedVelocity;
    private Rigidbody2D rb;
    private bool ballInPlay;
    private bool isPause = false;
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime);

        if (!isPause)
        {

            if (Input.GetButtonDown(Constants.FIRE_1) && !ballInPlay)
            {
                transform.parent = null;
                ballInPlay = true;
                rb.isKinematic = false;

                float additionalForce = GetIncreasedForce();

                rb.AddForce(new Vector3(
                    GetRandomBoolNumber() * Constants.BALL_INITIAL_VELOCITY + additionalForce,
                    Constants.BALL_INITIAL_VELOCITY + additionalForce, 0f));
            }
        }
    }

    private int GetRandomBoolNumber()
    {
        return Random.Range(0, 2) == 0 ? 1 : -1;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag.Equals("Wall"))
        {
            PushBall();
        }
    }

    /// <summary>
    /// Prevent endless bouncing between left and right wall.
    /// </summary>
    public void PushBall()
    {
        if (ballInPlay)
        {
            if (Math.Abs(rb.velocity.normalized.x) > 0.95f)
            {
                rb.AddForce(new Vector3(0, Random.Range(-50f, 50f), 0f));
            }
        }
    }

    private float GetIncreasedForce()
    {
        return 100f * GameManager.instance.Level.LevelNumber;
    }

    public void OnPauseGame()
    {
        isPause = true;
        savedVelocity = rb.velocity;
        rb.velocity = Vector3.zero;
    }

    public void OnResumeGame()
    {
        isPause = false;
        rb.velocity = savedVelocity;
    }
}
