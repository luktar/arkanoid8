﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

    public static bool newGamePressed = true;
    public static string playerName = Constants.DEFAULT_PLAYER_NAME;

    public static MenuManager instance;

    public Canvas mainMenuCanvas;
    public Canvas leaderBoardCanvas;
    public InputField playerNameInput;
    public Button newGameButton;
    public Button resumeButton;
    public Button exitButton;
    public Button leaderBoardButton;
    public Text leaderBoardText;

    private LeaderBoard leaderBoard;
    private Level level;

    void Awake () {

        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        level = new Level();
        leaderBoard = new LeaderBoard();
        leaderBoard.Load();

        newGameButton.onClick.AddListener(NewGameOnClick);
        resumeButton.onClick.AddListener(ResumeOnClick);
        exitButton.onClick.AddListener(ExitOnClick);
        leaderBoardButton.onClick.AddListener(LeaderBoardOnClick);

        playerNameInput.text = leaderBoard.PlayerName;
        playerNameInput.characterLimit = Constants.MAX_PLAYER_NAME_LENGTH;
        playerNameInput.onEndEdit.AddListener(PlayerNameEndEdit);

        mainMenuCanvas.gameObject.SetActive(true);
        leaderBoardCanvas.gameObject.SetActive(false);

        SetUpResumeButton();
    }
	
    /// <summary>
    /// Work around for button styling.
    /// </summary>
    private void SetUpResumeButton()
    {
        if (!level.Exists())
        {
            var colors = resumeButton.colors;
            colors.disabledColor = Color.black;
            resumeButton.colors = colors;
            resumeButton.enabled = false;

            Text buttonText = resumeButton.GetComponentInChildren<Text>();
            buttonText.color = Color.gray;
            return;
        }
        resumeButton.colors = newGameButton.colors;
        resumeButton.enabled = true;  
    }

    void LeaderBoardOnClick()
    {
        leaderBoard.Load();
        leaderBoardText.text = leaderBoard.ToString();

        leaderBoardCanvas.gameObject.SetActive(true);
    }

    void PlayerNameEndEdit(string value)
    {
        string playerName = value;
        if(string.IsNullOrEmpty(playerName))
        {
            playerName = Constants.DEFAULT_PLAYER_NAME;
            playerNameInput.text = Constants.DEFAULT_PLAYER_NAME;
        }
        leaderBoard.PlayerName = playerName;
        leaderBoard.Save();
    }

    void NewGameOnClick()
    {
        Level level = new Level();
        level.Remove();
        playerName = playerNameInput.text;
        newGamePressed = true;
        SceneManager.LoadScene(Constants.GAME_SCENE_NAME, LoadSceneMode.Single);
    }

    void ResumeOnClick()
    {
        playerName = string.Empty;
        newGamePressed = false;
        SceneManager.LoadScene(Constants.GAME_SCENE_NAME, LoadSceneMode.Single);
    }

    void ExitOnClick()
    {
        Application.Quit();
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            leaderBoardCanvas.gameObject.SetActive(false);
            SetUpResumeButton();
        }
    }

}
